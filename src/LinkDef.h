//#include "../xRooFit/xRooFit.h"

//#define XROOFIT_USE_PRAGMA_ONCE

#pragma link C++ class xRooFit+;
#pragma link C++ class xRooNode+;
#pragma link C++ class xRooNode::InteractiveObject+;
#pragma link C++ class xRooNLLVar+;
#pragma link C++ class xRooBrowser+;
#pragma link C++ class xRooHypoSpace+;
#pragma link C++ class xRooFit::StoredFitResult+;
//#pragma link C++ class xRooHypoPoint+;
